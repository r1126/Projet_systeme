#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
import fileinput
#a library that specifies a set of strings that matches it
import re


## I/ Vérification du fichier sam

if len(sys.argv)>1: #Vérifie si un paramètre en entrée.
    if os.path.isfile(sys.argv[1]) and os.path.exists(sys.argv[1]): #Vérifie que ce paramètre soit un fichier et que ce fichier existe.
        if os.stat(sys.argv[1]).st_size==0: # Verifie si ce fichier est non vide auquel cas le programme s'arrête.
            print("c'est un fichier vide")
            exit()
            
        else:            # VERIFICATION DU FICHIER SAM
                 file=open(sys.argv[1],"r") # Ouverture du fichier en entrée  (argument "r" = lecture seulement, impossible d'y écrire).
                 lines=file.readlines() #Lecture par ligne du fichier 
                 header=[]  #Création de  listes dans lesquelles on stocke certaines lignes du fichier en entrée.
                 Body=[]
                 for line in lines:
                    if line.startswith("@"):    #Ligne commencant par un @, correspondant aux lignes de l' entête d'un fichier SAM, sont stocké dans la liste header
                        header.append(line)
                    else:                      # Ligne sans @, correspondant au corps, sont stocké dans la liste Body
                        Body.append(line)
                 if  len(header)==0 or len(Body)==0: # Les deux listes doivent être non vides, auquel cas le fichier en entrée peut être considéré comme corrompu.
                     print("fichier corrompu")
                     exit()
                 if len(header) >2 : #L'entête du fichier sam en entré ne doit pas être plus long que 2 lignes, auquel cas le fichier en entrée peut être considéré comme corrompu.
                     print("fichier corrompu")
                     exit()
                 else :
                     print("fichier sam!")   
    else:
        print("Pas un fichier")
        exit()
        
        
else:
    print ("pas de fichier en entré")
    exit()

   
## II/.Stockage  des données du fichier SAM

#Initialisation de listes dans lesquelles certains elements du fichier SAM sont stockés
cigar=[]
flag=[]
sequence=[]
qname=[]
sequence_list=[]
qname_list=[]

#Initialisation de listes dans lesquelles certaines reads du fichier SAM sont stockés
## Paire de sequence non mapper (code1)
seqlist_paired_unmapped=[]    
name_paired_unmapped=[]

#Les reads partiellement mapper qui sont associé à un read non mapper (les reads non mapper sont absent de cette liste) (code2)
seqlis_partiellementmap_without_seq_of_unmapper_mate=[]
name_partiellementmap_without_seq_of_unmapper_mate=[]

#Les reads  mapper qui sont associé à un read non mapper (les reads non mapper sont absent de cette liste) (code3)
seqlis_mapper_without_seq_of_unmapper_mate=[]
name_mapper_without_seq_of_unmapper_mate=[]

#Les reads non mapper associé au reads partiellement ou totalement mapper ( associée au reads stocker dans code 2 ou code 3) (code4)
seqlist_unmapped_solo=[]
name_unmapped_solo=[]

#Les paires de reads mapper (partiellement ou totalment) (code5)
seqlist_paired_mapped=[]
name_paired_mapped=[]

# Les reads partiellement mapper associé à un reads totalement  mapper  (code6)
seqlist_partiel_mate_mapped=[]
name_partiel_mate_mapped=[]

#Les reads totalement mapper associé à un read  partiellement mapper (code7)
seqlist_mapper_mate=[]
name_mapper_mate=[]


#Filtre des reads sur la base de leurs FLAG ET CIGAR

for ligne in Body :
    flag=int(ligne.split('\t')[1]) # On associe à la liste précédement créée les valeurs Flag prisé par les différentes reads du fichier sam. La fonction split permet de découpé la ligne en plusieurs colonnes,  [1]=position de l'element flag dans la ligne. 
    cigar=ligne.split('\t')[5]
    sequence=ligne.split('\t')[9]
    qname=ligne.split('\t')[0]
    
    if (flag > 2048) :      # Décomposition du FLAG via les différentes valeurs informatives issues de la table 2 (cf Rapport Penel-Rosier)
        flag = flag - 2048  # On soustrait au Flag la valeurs la plus importante possible et cela jusqu'à que cela ne soit plus possible.
    if ( flag >= 1024):
        flag = flag - 1024
    if (flag >= 512):
        flag = flag- 512
    if (flag >= 256 ):
        flag =flag - 256
    if (flag >= 128):
        flag =flag- 128
    if ( flag >= 64 ):
        flag =flag- 64
    if (flag >= 32 ):
        flag =flag- 32
    if ( flag >= 16 ):
        flag =flag- 16
    if (flag >= 8):    # Si le flag d'une read donnée est soustrayable par 8 cela indique que sont mate est non mapper
        flag =flag- 8
        if ( flag >= 4 ) :   # Si ce même flag est soustrayable par 4, alors cela indique que cette séquence est elle aussi non mapper.
            flag = flag-4
            seqlist_paired_unmapped.append(sequence) #(code1) On stock dans cette liste les sequences qui sont non mapper et dont le mate est non mapper aussi.
            name_paired_unmapped.append(qname) #On stock le nom des sequences qui sont non mapper et dont le mate est non mapper aussi.
        else :     # Si le flag est soustrayble par 8 mais pas par 4, alors cela veut dire que la séquence en question est mapper ( partiellement ou totalement) mais que sont mate non
            if (cigar !=100) :  #Si le cigar associé à la sequence qui avait un flag soustrayble par 8 mais pas par 4 est différent de 100M, alors la séquence est partiellement mapper
                seqlis_partiellementmap_without_seq_of_unmapper_mate.append(sequence) #(code2)
                name_partiellementmap_without_seq_of_unmapper_mate.append(qname)
            else: # Dans le cas contraire, la sequence est totalement mapper mais a un mate non mapper
                seqlis_mapper_without_seq_of_unmapper_mate.append(sequence) #(code3)
                name_mapper_without_seq_of_unmapper_mate.append(name)
                
    if (flag >=4): #Si le flag d'un read donnée à un flag soustrayable par 4 mais pas par 8, alors il s'agit d'un reads  non mapper qui est associé à un read mapper (partiellement(code2) ou totalement(code3)).
        flag=flag - 4 
        seqlist_unmapped_solo.append(sequence) #(code4)
        name_unmapped_solo.append(qname)
    if (flag >=2): # Si le flag d'un read est soustrayable par 2, alors le read  est mapper (totalement ou partiellement) et sont mate l'est aussi (totalement ou partiellement).
        flag=flag -2
        seqlist_paired_mapped.append(sequence) #(code 5)
        name_paired_mapped.append(qname)
        if (cigar != "100M"): # Si le cigar associé à ce read  est différent de 100M alors il est partiellement mapper.
            seqlist_partiel_mate_mapped.append(sequence) #liste des reads partiellement mapper associé à un read mapper. 
            name_partiel_mate_mapped.append(qname) # liste des nom des reads partiellement mapper associé à un read mapper.Cette liste (code6) très importante pour la suite.
            
for ligne in Body:
    flag=int(ligne.split('\t')[1]) # on recommence le processus précédent, sans s'attardé sur les flag 8 et 4
    cigar=ligne.split('\t')[5]
    sequence=ligne.split('\t')[9]
    qname=ligne.split('\t')[0]
    if (flag > 2048) :
        flag = flag - 2048
    if ( flag >= 1024):
        flag = flag - 1024
    if (flag >= 512):
        flag = flag- 512
    if (flag >= 256 ):
        flag =flag - 256
    if (flag >= 128):
        flag =flag- 128
    if ( flag >= 64 ):
        flag =flag- 64
    if (flag >= 32 ):
        flag =flag- 32
    if ( flag >= 16 ):
        flag =flag- 16
    if (flag >= 8):
        flag =flag- 8
    if ( flag >= 4 ) :
        flag = flag-4
    if ( flag >= 2 ) : 
        flag = flag - 2
        if (( cigar=="100M") and ( qname in name_partiel_mate_mapped )): # si le flag d'un read donné est soustrayable par 2, que le cigar associé est égal à 100M et que sont nom est présent dans la liste code6 alors on a trouvé les séquences totalement mapper associé au séquence partiellement mapper.
            if ( sequence not in seqlist_partiel_mate_mapped ) :
              seqlist_mapper_mate.append(sequence) #(code6)
              name_mapper_mate.append(qname)
                    


###ANALYSE ET SORTIES.

### Attention si l'un des listes crée est vide, certains soucis peuvent être rencontré lors de la concatenation de certaines liste pour crée des fichiers en format fasta. Verifier à l'aide de l'argument print(len(liste) qu'elle liste pourrait être préoccupante.
## ICI avec le fichier mapping.sam, la liste name_mapper_without_seq_of_unmapper_mat est vide, et donc empeche la création du fichier faste de la partie 3-

### 1-  NON MAPPER   CREATION D'UN FICHIER FASTA AVEC LES PAIRES DE READS NON MAPPER.
#Paire de reads non mappé ( code1)
for seq in range (len(name_paired_unmapped)): 
    myfile = open("0-unmapped_readpairs.fasta","a+") #Je crée une sorti dans lequel je vais pouvoir écrire des informations
    myfile.write("> " + name_paired_unmapped[seq] + '\n' + seqlist_paired_unmapped[seq]+ '\n' ) # J'implemente les noms et les sequences associées au reads qui sont non mapper en paires.
    myfile.close()
#reads seuls non mapper (code 4)
for seq in range (len(name_unmapped_solo)):
    myfile = open("unmapper_mate.fasta","a+")
    myfile.write("> " + name_unmapped_solo[seq] + '\n' +seqlist_unmapped_solo[seq]+ '\n' )
    myfile.close()
#Read non mappé (fusion des listes code 1 et code 4)
myfile = open("0-unmapped_readpairs.fasta")
data=myfile.read()
myfile2 = open("unmapper_mate.fasta")
data2=myfile2.read()
data += data2
myfile3 = open("1-unmap.fasta","a+")
myfile3.write(data)

### 2-  Partiellement MAPPER
#reads partiellement mappé associé à des reads non mapper(code 2)
for seq in range (len(name_partiellementmap_without_seq_of_unmapper_mate)):
    myfile = open("partiellementmap_mate.fasta","a+")
    myfile.write("> " + name_partiellementmap_without_seq_of_unmapper_mate[seq] + '\n' +seqlis_partiellementmap_without_seq_of_unmapper_mate[seq]+ '\n')
    myfile.close()
#reads partiellement mappé associée à des reads mapper ou partiellement mapper (code6)
for seq in range (len(name_partiel_mate_mapped)):
    myfile = open("partiel_mate.fasta","a+")
    myfile.write("> " + name_partiel_mate_mapped[seq] + '\n' +seqlist_partiel_mate_mapped[seq]+ '\n' )
    myfile.close()


#Ensemble des reads partiellement mappé (code 2 et code 6)
myfile = open("partiel_mate.fasta")
data = myfile.read()
myfile2 = open("partiellementmap_mate.fasta")
data2 = myfile2.read()
data += data
myfile3 = open("2-partiellementmap.fasta","a+")
myfile3.write(data)


## 3- reads NON mapper et leur mate partiellement mapper ou mapper .
#Sequence mappé avec un mate non mappé (code 3)
#for seq in range (len(name_mapper_without_seq_of_unmapper_mate)):
#    myfile = open("mapper_mate.fasta","a+")
#    myfile.write("> " + name_mapper_without_seq_of_unmapper_mate[seq] + '\n' +seqlis_mapper_without_seq_of_unmapper_mate[seq]+ '\n')
#    myfile.close()
    

myfile = open("partiellementmap_mate.fasta") #code 2
data = myfile.read()
myfile2 = open("unmapper_mate.fasta") #code 4
data2=myfile2.read()
data += data2
#myfile3=open("mapper_mate.fasta") #code3
#data3=myfile3.read()
#data +=data3
myfile4=open("3-Unmapped_and_mappermate_or_partialmappermate.fasta","a+") # code 2 3 4 ensemble
myfile4.write(data)


##4- reads  mapper avec reads partiellement mapper
for seq in range (len(name_mapper_mate)):
    myfile = open("mapper_mate.fasta","a+")
    myfile.write("> " + name_mapper_mate[seq] + '\n' +seqlist_mapper_mate[seq]+ '\n')
    myfile.close()

myfile=open("partiel_mate.fasta") #code 6
data=myfile.read()
myfile2=open("mapper_mate.fasta") #code 7
data2 =myfile2.read()
data +=  data2
myfile3=open("4-mapper_partiel_paires.fasta","a+") #code 6 et 7 associée
myfile3.write(data)

                    

## 0-Paire de sequence mappé(code5) ( READ peut etre mapper à 100% ou non / très long attention !)
#for seq in range (len(name_paired_mapped)):
#   myfile = open("0-mapped_readpairs.fasta","a+")
#   myfile.write("> " + name_paired_mapped[seq] + '\n' + seqlist_paired_mapped[seq]+ '\n')
#    myfile.close()


# Analyse simple du fichier

A=(len(Body))
B=(len(name_mapper_without_seq_of_unmapper_mate)+len(name_paired_unmapped))
C=(len(name_paired_unmapped)/2)
D=(len(name_partiellementmap_without_seq_of_unmapper_mate) +len(name_partiel_mate_mapped))
E=(len(name_paired_mapped))
F=((E/A) * 100)
H=((C/A) * 100)
myfile=open("0-ANALYSE_SAM.TXT","a+")
myfile.write( "Le nombre de reads contenu dans ce fichier SAM : ")
myfile.write (str(A) + '\n')
myfile.write( "Le nombre de reads non mappé : ")
myfile.write(str(B) +'\n')
myfile.write("Ce qui correspond à " + str(H) + "% des reads du fichier SAM" +'\n')
myfile.write("Le nombre de paires de reads non mappé : ")
myfile.write(str(C) +'\n')
myfile.write("Le nombre de reads partiellement mappé : ")
myfile.write( str(D) +'\n')
myfile.write ("Le nombre de reads mappé : ")
myfile.write(str(E) +'\n')
myfile.write("Ce qui correspond à " + str(F) + "% des reads du fichier SAM" +'\n')

myfile.write("6 fichier fasta accompagne ce document texte :" +'\n'+ "0-unmapped_readpairs.fasta"+ '\n' + "1-unmap.fasta" + '\n'+ "2-partiellementmap.fasta" + '\n' + "3-Unmapped_and_mappermate_or_partialmappermate.fasta" +'\n' + "4-mapper_partiel_paires.fasta" + '\n' +"0-mapped_readpairs.fasta")
myfile.close()
