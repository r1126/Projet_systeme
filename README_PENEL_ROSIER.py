#!/usr/bin/python3
#-*- coding : utf-8 -*-

############### README  ###############
__authors__ = ("PENEL BENOIT", "ROSIER ALEX")
__contact__ = ("benoit.penel@etu.umontpellier.fr","alex.rosier@etu.umontpellier.fr")
__version__ = "1.0"
__date__ = "12/14/2021"
__licence__ ="This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
        GNU General Public License for more details.
        You should have received a copy of the GNU General Public License
        along with this program. If not, see <https://www.gnu.org/licenses/>."

    ### OPTION LIST:
        ##-h or --help : help information
        ##-i or --input: input file (.sam)
        ##-o or --output: output name files (.txt)

    #Synopsis:
        ##Reader.py -h or --help # launch the help.
        ##Reader.py -i or --input <file> # Launch SamReader to analyze a samtools file (.sam) and print the result in the terminal
        ##Reader.py -i or --input <file> -o or --output <name> # Launch SamReader to analyze a samtools file (.sam) and print the result in the file called <name>
  
############### IMPORT MODULES ###############

import sys , os,  fileinput

############### FUNCTIONS TO :

## 0/ Run :

-Le programme associé à ce README est encodé en langage PYTHON3. Assurez-vous d'utiliser PYTHON3 pour exécuter le script associé.
-Le script prend en paramètre d'entrée un fichier au format SAM, veillez à ce que cela soit le cas et que le fichier ne soit pas corrompu.
Auquel cas un message d'erreur indiquant l'erreur sera retourné.
-Les reads qui proviennent d'une même séquence doivent être désignées par le même identifiant, sans suffixe permettant leur distinction type : "_1" ou "_2".
Auquel cas le programme ne sera pas capable de regrouper les séquences par paires dans les différentes sorties format fasta créées.

Pour récuperer le script sur gitlab :
git clone https://gitlab.com/r1126/Projet_systeme.git

Effectuez cette commande pour rendre le fichier executable :
chmod +x Script.py

Afin d'exécuter le programme, exécuté la ligne suivante dans un terminale:
./Script.py file.sam

## 1/ Check :

script.py est un script qui prend en parmètre un fichier texte de format SAM pour en extraire des informations. 
La première partie du script est donc consacré à l'analyse du fichier mit en paramètre.

## 2/ Read, 

Le temps de lecture du fichier pris en paramètre dépend de sa taille.
A chaque fois que vous voulez lancer le script prenez bien le temps de supprimer les anciens fichiers créés :
rm *.fasta
rm *.TXT

## 3/ Store,

Le script.py créer un fichier au format txt renseignant les résultats de l'analyse.
Le script.py créer des fichiers au format fasta en output, dont le nom du fichier renseigne sur le contenu extrait du fichier prit en paramètre.

## 4/ Analyse 

Le script.py permet l'analyse par le flag mais pas par le cigar.
L'analyse simple du fichier est stocké dans 0-ANALYSE_SAM.TXT
Ce fichier répertorie tous les fichiers au format fasta créés.

 
#### Summarise the results ####

1 fichier au format .txt
9 fichiers au format .fasta

